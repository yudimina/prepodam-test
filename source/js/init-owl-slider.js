// owl carousel init

"use strict";

	function prepodsCarousel() {
		var checkWidth = $(window).width();
		var owlPost = $("#prepodsOwl .content__prepods-carousel");
		if (checkWidth > 1020) {
			if(typeof owlPost.data('owl.carousel') != 'undefined'){
				owlPost.data('owl.carousel').destroy();
			}
			owlPost.removeClass('owl-carousel');
		} else if (checkWidth < 1024) {
			owlPost.addClass('owl-carousel');
			owlPost.owlCarousel({
        center: true,
        items: 1,
        loop: true,
        margin: 15,
        autoWidth: true
			});
		}
	}

  prepodsCarousel();
  $(window).resize(prepodsCarousel);
