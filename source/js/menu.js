// menu-btn + menu

$(document).ready(function() {
  $('.menu-btn').click(function() {
    var menu = document.querySelector('.navigation');
    $(this).toggleClass('menu-btn--open-js');
    $(menu).toggleClass('navigation--opened-js');
  });
});
